package cat.gergokajtar.privaliatest.data.repository

import cat.gergokajtar.privaliatest.data.mapper.JsonMovieMapper
import cat.gergokajtar.privaliatest.data.mapper.JsonResponsePageMapper
import cat.gergokajtar.privaliatest.data.model.json.JsonMovie
import cat.gergokajtar.privaliatest.data.model.view.Movie
import cat.gergokajtar.privaliatest.data.model.view.Page
import cat.gergokajtar.privaliatest.data.source.RemoteMovieDataSource
import io.reactivex.Single
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(val remoteMovieDataSource: RemoteMovieDataSource) : MovieRepository {

    override fun getPopularMovies(nextPage: Int): Single<Page<Movie>> {
        return remoteMovieDataSource.getPopularMovies(nextPage)
                .map {
                    jsonResponsePage ->
                    JsonResponsePageMapper<JsonMovie, Movie>(JsonMovieMapper()).apply(jsonResponsePage) }
    }

    override fun getMoviesByQuery(nextPage: Int, query: String): Single<Page<Movie>> {
        return remoteMovieDataSource.getMoviesByQuery(nextPage, query)
                .map {
                    jsonResponsePage ->
                    JsonResponsePageMapper<JsonMovie, Movie>(JsonMovieMapper()).apply(jsonResponsePage) }
    }
}