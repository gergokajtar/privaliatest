package cat.gergokajtar.privaliatest.data.mapper

import cat.gergokajtar.privaliatest.data.model.json.JsonImageConfiguration
import cat.gergokajtar.privaliatest.data.model.json.JsonMovie
import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import cat.gergokajtar.privaliatest.data.model.view.Movie
import io.reactivex.functions.Function

class JsonImageConfigurationMapper : Function<JsonImageConfiguration, ImageConfiguration> {
    override fun apply(jsonConfig: JsonImageConfiguration): ImageConfiguration {
        return ImageConfiguration(jsonConfig.secureBaseUrl,
                jsonConfig.posterSizes.map {
                    when (it) {
                        "original" -> Int.MAX_VALUE to it
                        else -> it.substring(1).toInt() to it // Assuming convention of w* for poster sizes
                    }
                })
    }
}