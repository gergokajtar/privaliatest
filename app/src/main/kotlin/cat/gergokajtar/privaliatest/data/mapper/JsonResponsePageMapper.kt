package cat.gergokajtar.privaliatest.data.mapper

import cat.gergokajtar.privaliatest.data.model.json.ApiModel
import cat.gergokajtar.privaliatest.data.model.json.JsonResponsePage
import cat.gergokajtar.privaliatest.data.model.view.Page
import io.reactivex.functions.Function

class JsonResponsePageMapper<T : ApiModel, R>(val itemMapper: Function<T, R>) : Function<JsonResponsePage<T>, Page<R>> {

    override fun apply(jsonResponsePage: JsonResponsePage<T>): Page<R> {
        return Page<R>(jsonResponsePage.page,
                jsonResponsePage.results.map { itemMapper.apply(it) },
                jsonResponsePage.page < jsonResponsePage.totalPages)
    }
}