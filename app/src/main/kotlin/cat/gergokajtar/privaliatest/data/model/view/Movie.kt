package cat.gergokajtar.privaliatest.data.model.view

data class Movie(var id: Long,
                  var title: String,
                  var posterPath: String?,
                  var overview: String,
                  var releaseYear: String)