package cat.gergokajtar.privaliatest.data.model.json

import com.google.gson.annotations.SerializedName

data class JsonResponsePage<T : ApiModel>(
        @SerializedName("page") val page: Int,
        @SerializedName("total_results") val totalResults: Int,
        @SerializedName("total_pages") val totalPages: Int,
        @SerializedName("results") val results: List<T>
)