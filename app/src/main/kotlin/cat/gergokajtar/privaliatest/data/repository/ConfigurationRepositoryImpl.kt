package cat.gergokajtar.privaliatest.data.repository

import cat.gergokajtar.privaliatest.data.mapper.JsonImageConfigurationMapper
import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import cat.gergokajtar.privaliatest.data.source.RemoteConfigurationDataSource
import io.reactivex.Single
import javax.inject.Inject

class ConfigurationRepositoryImpl @Inject constructor(val remoteConfigurationDataSource: RemoteConfigurationDataSource) : ConfigurationRepository {
    override fun getImageConfiguration(): Single<ImageConfiguration> {
        return remoteConfigurationDataSource.getConfiguration()
                .map { JsonImageConfigurationMapper().apply(it.images) }
    }

}