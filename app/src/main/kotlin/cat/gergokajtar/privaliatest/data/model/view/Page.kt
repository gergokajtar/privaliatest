package cat.gergokajtar.privaliatest.data.model.view

class Page<T>(val pageNumber: Int,
              val items: List<T>,
              val hasNext: Boolean)