package cat.gergokajtar.privaliatest.data.source

import cat.gergokajtar.privaliatest.data.model.json.JsonConfiguration
import io.reactivex.Single
import retrofit2.http.GET

interface RemoteConfigurationDataSource {

    @GET("configuration")
    fun getConfiguration(): Single<JsonConfiguration>
}