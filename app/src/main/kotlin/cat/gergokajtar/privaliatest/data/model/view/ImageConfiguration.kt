package cat.gergokajtar.privaliatest.data.model.view

class ImageConfiguration(val baseUrl: String,
                         val posterSizes: List<Pair<Int, String>>) {

    fun getPosterSizeForWidth(width: Int): String {
        var posterSize: String = posterSizes.last().second
        loop@ for (size in posterSizes) {
            if (width <= size.first) {
                posterSize = size.second
                break@loop
            }
        }
        return posterSize
    }
}