package cat.gergokajtar.privaliatest.data.model.json
import com.google.gson.annotations.SerializedName


data class JsonConfiguration(
		@SerializedName("images") var images: JsonImageConfiguration,
		@SerializedName("change_keys") var changeKeys: List<String>
)