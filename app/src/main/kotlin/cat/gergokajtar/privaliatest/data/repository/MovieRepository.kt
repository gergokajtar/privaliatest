package cat.gergokajtar.privaliatest.data.repository

import cat.gergokajtar.privaliatest.data.model.view.Movie
import cat.gergokajtar.privaliatest.data.model.view.Page
import io.reactivex.Single

interface MovieRepository {
    fun getPopularMovies(nextPage: Int): Single<Page<Movie>>
    fun getMoviesByQuery(nextPage: Int, query: String): Single<Page<Movie>>
}