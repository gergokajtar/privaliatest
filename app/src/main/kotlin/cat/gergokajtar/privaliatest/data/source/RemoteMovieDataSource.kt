package cat.gergokajtar.privaliatest.data.source

import cat.gergokajtar.privaliatest.data.model.json.JsonMovie
import cat.gergokajtar.privaliatest.data.model.json.JsonResponsePage
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteMovieDataSource {

    @GET("movie/popular")
    fun getPopularMovies(@Query("page") nextPage:Int): Single<JsonResponsePage<JsonMovie>>

    @GET("search/movie")
    fun getMoviesByQuery(@Query("page") nextPage:Int, @Query("query") query: String): Single<JsonResponsePage<JsonMovie>>
}