package cat.gergokajtar.privaliatest.data.mapper

import cat.gergokajtar.privaliatest.data.model.json.JsonMovie
import cat.gergokajtar.privaliatest.data.model.view.Movie
import io.reactivex.functions.Function

class JsonMovieMapper : Function<JsonMovie, Movie> {
    override fun apply(jsonMovie: JsonMovie): Movie {
        return Movie(jsonMovie.id,
                jsonMovie.title,
                jsonMovie.posterPath,
                jsonMovie.overview,
                when (jsonMovie.releaseDate) {
                    null, "" -> "N/A"
                    else -> jsonMovie.releaseDate.substring(0, 4)
                })
    }
}