package cat.gergokajtar.privaliatest.data.repository

import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import io.reactivex.Single

interface ConfigurationRepository {

    fun getImageConfiguration(): Single<ImageConfiguration>
}