package cat.gergokajtar.privaliatest.di

import android.app.Application
import cat.gergokajtar.privaliatest.data.source.interceptor.ApiKeyInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    private val CACHE_SIZE_BYTES = 1024 * 1024 * 2L
    private val CACHE_DIR = "rest-cache"

    @Provides
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
    }

    @Provides
    fun provideOkHttpClient(app: Application): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(ApiKeyInterceptor())
                .cache(Cache(File(app.cacheDir, CACHE_DIR) , CACHE_SIZE_BYTES))
                .build()
    }
}