package cat.gergokajtar.privaliatest.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import cat.gergokajtar.privaliatest.feature.list.MovieListViewModel
import cat.gergokajtar.privaliatest.viewmodel.MovieViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel::class)
    abstract fun bindMovieListViewModel(movieListViewModel: MovieListViewModel): ViewModel

    @Binds
    abstract fun bindMovieViewModelFactory(factory: MovieViewModelFactory): ViewModelProvider.Factory
}
