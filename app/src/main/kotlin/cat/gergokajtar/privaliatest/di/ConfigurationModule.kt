package cat.gergokajtar.privaliatest.di

import cat.gergokajtar.privaliatest.data.repository.ConfigurationRepository
import cat.gergokajtar.privaliatest.data.repository.ConfigurationRepositoryImpl
import cat.gergokajtar.privaliatest.data.source.RemoteConfigurationDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ConfigurationModule {

    @Provides
    fun provideConfigurationRepository(remoteConfigurationDataSource: RemoteConfigurationDataSource): ConfigurationRepository =
            ConfigurationRepositoryImpl(remoteConfigurationDataSource)

    @Provides
    fun provideRemoteConfigurationDataSource(retrofit: Retrofit): RemoteConfigurationDataSource =
            retrofit.create(RemoteConfigurationDataSource::class.java)
}