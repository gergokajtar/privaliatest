package cat.gergokajtar.privaliatest.di

import cat.gergokajtar.privaliatest.data.repository.MovieRepository
import cat.gergokajtar.privaliatest.data.repository.MovieRepositoryImpl
import cat.gergokajtar.privaliatest.data.source.RemoteMovieDataSource
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class MovieModule {

    @Provides
    fun provideMovieRepository(remoteMovieDataSource: RemoteMovieDataSource): MovieRepository =
            MovieRepositoryImpl(remoteMovieDataSource)

    @Provides
    fun provideRemoteMovieDataSource(retrofit: Retrofit): RemoteMovieDataSource =
            retrofit.create(RemoteMovieDataSource::class.java)
}