package cat.gergokajtar.privaliatest.feature.list

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cat.gergokajtar.privaliatest.R
import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import cat.gergokajtar.privaliatest.data.model.view.Movie

class MoviesAdapter : RecyclerView.Adapter<MovieViewHolder>() {

    var movies: MutableList<Movie> = ArrayList<Movie>()
    var imageConfiguration: ImageConfiguration? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val movieView = LayoutInflater.from(parent.context).inflate(R.layout.viewholder_movie, parent, false)
        return MovieViewHolder(movieView, imageConfiguration)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount(): Int = movies.size

    fun setItems(newItems: List<Movie>) {
        val diffResults = DiffUtil.calculateDiff(MovieListDiffUtil(movies, newItems))

        movies.clear()
        movies.addAll(newItems)

        diffResults.dispatchUpdatesTo(this)
    }

    class MovieListDiffUtil(val oldList: List<Movie>, val newList: List<Movie>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id.equals(newList[newItemPosition].id)
        }

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            areItemsTheSame(oldItemPosition, newItemPosition)
    }
}