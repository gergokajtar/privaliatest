package cat.gergokajtar.privaliatest.feature.list

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import cat.gergokajtar.privaliatest.data.model.view.Movie
import cat.gergokajtar.privaliatest.data.model.view.Page
import cat.gergokajtar.privaliatest.data.repository.ConfigurationRepository
import cat.gergokajtar.privaliatest.data.repository.MovieRepository
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieListViewModel @Inject constructor(val movieRepository: MovieRepository,
                                             val configurationRepository: ConfigurationRepository) : ViewModel() {

    var moviesToShow = MutableLiveData<List<Movie>>()
    private var movies: MutableList<Movie> = ArrayList()
    var moviesObserver: Disposable? = null

    var imageConfiguration = MutableLiveData<ImageConfiguration>()

    var loading = MutableLiveData<Boolean>()
    var error = MutableLiveData<Throwable>()

    var query: String = ""
    var nextPageToLoad: Int
    var hasNext: Boolean

    var successConsumer: Consumer<Page<Movie>> = Consumer {
        loading.postValue(false)
        hasNext = it.hasNext
        if (it.pageNumber == 1) { movies.clear() }
        movies.addAll(it.items)

        moviesToShow.postValue(movies)
        nextPageToLoad = it.pageNumber + 1
        moviesObserver?.dispose() }

    var errorConsumer: Consumer<Throwable> = Consumer {
        loading.postValue(false)
        error.postValue(it)
        moviesObserver?.dispose()}

    init {
        hasNext = true
        nextPageToLoad = 1
        loadConfiguration()
    }

    fun loadConfiguration() {
        configurationRepository.getImageConfiguration()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnSuccess { imageConfiguration.postValue(it) }
                .map { movieRepository.getPopularMovies(nextPageToLoad).blockingGet() }
                .subscribe(successConsumer, errorConsumer)
    }

    fun refresh() {
        if (moviesObserver?.isDisposed ?: true) {
            reset()
            loadNextPage()
        }
    }

    fun loadNextPage() {
        if (!hasNext) return

        loading.value = true
        moviesObserver = getMoviesObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(successConsumer, errorConsumer)
    }

    fun onQueryTextChanged(newQuery:String) {
        if (!query.equals(newQuery)) {
            query = newQuery;
            forceRefresh()
        }
    }

    private fun forceRefresh() {
        loading.value = false
        moviesObserver?.dispose()
        reset()

        loadNextPage()
    }

    private fun getMoviesObservable(): Single<Page<Movie>> =
        when {
            query.isEmpty() -> movieRepository.getPopularMovies(nextPageToLoad)
            else -> movieRepository.getMoviesByQuery(nextPageToLoad, query)
        }

    private fun reset() {
        nextPageToLoad = 1
        hasNext = true
    }

    override fun onCleared() {
        moviesObserver?.dispose()
    }
}