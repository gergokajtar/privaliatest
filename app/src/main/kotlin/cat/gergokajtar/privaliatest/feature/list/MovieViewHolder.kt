package cat.gergokajtar.privaliatest.feature.list

import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.view.doOnLayout
import cat.gergokajtar.privaliatest.R
import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import cat.gergokajtar.privaliatest.data.model.view.Movie
import com.squareup.picasso.Picasso

class MovieViewHolder(itemView: View, val imageConfiguration: ImageConfiguration?) : RecyclerView.ViewHolder(itemView) {

    val posterImageView: ImageView
    val titleTextView: TextView
    val overviewTextView: TextView

    val titleStringFormat: String
    var baseImageUrl: String? = null

    init {
        posterImageView = itemView.findViewById(R.id.imageview_poster)
        titleTextView = itemView.findViewById(R.id.textview_movie_title)
        overviewTextView = itemView.findViewById(R.id.textview_movie_overview)

        titleStringFormat = itemView.context.resources.getString(R.string.movie_title_and_year)
    }

    fun bind(movie: Movie) {
        titleTextView.text = String.format(titleStringFormat, movie.title, movie.releaseYear)
        overviewTextView.text = movie.overview
        movie.posterPath?.let { posterPath ->
            imageConfiguration?.baseUrl?.let { baseUrl ->
                if (posterImageView.width > 0) {
                    loadImage(baseUrl, posterPath)
                } else {
                    posterImageView.doOnLayout {
                        loadImage(baseUrl, posterPath)
                    }
                }
            }
        }
    }

    private fun loadImage(baseUrl: String, posterPath: String) {
        Picasso.with(itemView.context)
                .load(baseUrl + imageConfiguration?.getPosterSizeForWidth(posterImageView.width) + posterPath)
                .resize(posterImageView.width, posterImageView.height)
                .into(posterImageView)
    }
}