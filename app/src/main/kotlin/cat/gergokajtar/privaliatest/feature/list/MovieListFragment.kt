package cat.gergokajtar.privaliatest.feature.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import cat.gergokajtar.privaliatest.R
import cat.gergokajtar.privaliatest.data.model.view.ImageConfiguration
import cat.gergokajtar.privaliatest.data.model.view.Movie
import cat.gergokajtar.privaliatest.di.Injectable
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MovieListFragment : Fragment(), Injectable {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: MovieListViewModel

    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var movieRecyclerView: RecyclerView
    lateinit var moviesAdapter: MoviesAdapter

    lateinit var scrollListener: RecyclerView.OnScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieListViewModel::class.java)

        startObserveImageConfiguration()
        startObserveItems()
        startObserveLoading()
        startObserveError()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_list, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        movieRecyclerView = view.findViewById(R.id.recyclerview_list)

        movieRecyclerView.layoutManager = LinearLayoutManager(context)
        moviesAdapter = MoviesAdapter()
        movieRecyclerView.adapter = moviesAdapter
        scrollListener = object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                with (movieRecyclerView.layoutManager as LinearLayoutManager) {
                    val visibleItemCount = childCount
                    val totalItemCount = itemCount
                    val firstVisibleItemPosition = findFirstVisibleItemPosition()

                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount) {
                        movieRecyclerView.removeOnScrollListener(scrollListener)
                        viewModel.loadNextPage()
                    }
                }

            }

        }

        swipeRefreshLayout = view.findViewById(R.id.swiperefreshlayout_list)
        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refresh()
        }
    }

    private fun startObserveLoading() {
        viewModel.loading.observe(this,
                Observer { loading ->
                    swipeRefreshLayout.isRefreshing = loading == true
                })
    }

    private fun startObserveItems() {
        viewModel.moviesToShow.observe(this,
                Observer {
                    val newEvents = when {
                        it == null -> emptyList<Movie>()
                        else -> it
                    }
                    moviesAdapter.setItems(newEvents)
                    movieRecyclerView.addOnScrollListener(scrollListener)
                })
    }

    private fun startObserveError() {
        viewModel.error.observe(this,
                Observer { error ->
                    if (error != null) {
                        val mySnackbar = Snackbar.make(swipeRefreshLayout,
                                "There has been a problem.", Snackbar.LENGTH_INDEFINITE)
                        mySnackbar.setAction("Retry", {
                            mySnackbar.dismiss()
                            viewModel.refresh()
                        })
                        mySnackbar.show()
                    }
                })
    }

    private fun startObserveImageConfiguration() {
        viewModel.imageConfiguration.observe(this,
                Observer { config ->
                    moviesAdapter.imageConfiguration = config
                })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_search, menu)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        RxSearchView.queryTextChanges(searchView)
                .debounce(400, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { viewModel.onQueryTextChanged(it.toString()) }
    }
}